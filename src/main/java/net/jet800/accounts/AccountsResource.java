package net.jet800.accounts;

import io.dropwizard.hibernate.UnitOfWork;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

@Path("/accounts")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class AccountsResource {

  private final AccountStore accountStore;

  public AccountsResource(AccountStore dao) {
    accountStore = dao;
  }

  @POST
  @UnitOfWork
  public long create(Account account) {
    return accountStore.create(account);
  }

  @GET
  @Path("/{id}")
  @UnitOfWork
  public Account get(@PathParam("id") long id) {
    return accountStore.findById(id);
  }

  /**
   * Transfers amount of money from {@link net.jet800.accounts.Account} with transfer.sourceAccountId
   * to {@link Account} with specified id
   * if sourceAccount does have enough money.
   * @param id id of Account to add money to.
   * @param transfer money transfer parameters
   * @return true - if transfer was successful, false otherwise.
   */
  @POST
  @Path("/{id}/transfer")
  @UnitOfWork
  public boolean transfer(@PathParam("id") long id, MoneyTransfer transfer) {
    Account source = accountStore.findById(transfer.getSourceAccountId());
    Account destination = accountStore.findById(id);
    if (source.getBalance().compareTo(transfer.getAmount()) >= 0) {
      source.setBalance(source.getBalance().subtract(transfer.getAmount()));
      destination.setBalance(destination.getBalance().add(transfer.getAmount()));
      return true;
    }
    return false;
  }

}