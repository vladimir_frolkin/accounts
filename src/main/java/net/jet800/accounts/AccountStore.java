package net.jet800.accounts;

import io.dropwizard.hibernate.AbstractDAO;
import org.hibernate.SessionFactory;

class AccountStore extends AbstractDAO<Account> {
  AccountStore(SessionFactory factory) {
    super(factory);
  }

  Account findById(Long id) {
    return get(id);
  }

  long create(Account account) {
    return persist(account).getId();
  }

}
