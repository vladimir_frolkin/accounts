package net.jet800.accounts;

import io.dropwizard.Application;
import io.dropwizard.db.DataSourceFactory;
import io.dropwizard.hibernate.HibernateBundle;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;

public class AccountsService extends Application<AccountsConfiguration> {

  private final HibernateBundle<AccountsConfiguration> hibernate = new HibernateBundle<AccountsConfiguration>(Account.class) {
    @Override
    public DataSourceFactory getDataSourceFactory(AccountsConfiguration configuration) {
      return configuration.getDataSourceFactory();
    }
  };

  public static void main(String[] args) throws Exception {
    new AccountsService().run(args);
  }

  @Override
  public void initialize(Bootstrap<AccountsConfiguration> bootstrap) {
    bootstrap.addBundle(hibernate);
  }

  @Override
  public void run(AccountsConfiguration configuration,
                  Environment environment) {
    final AccountStore dao = new AccountStore(hibernate.getSessionFactory());
    environment.jersey().register(new AccountsResource(dao));
  }

}
