package net.jet800.accounts;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Data
@NoArgsConstructor
@AllArgsConstructor
class MoneyTransfer {
  @Valid
  @NotNull
  private long sourceAccountId;
  @Valid
  @NotNull
  private BigDecimal amount;
}
