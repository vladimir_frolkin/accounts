package net.jet800.accounts;

import io.dropwizard.testing.junit.ResourceTestRule;
import org.junit.After;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Test;

import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.math.BigDecimal;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

public class AccountsResourceTest {

  private static final AccountStore store = mock(AccountStore.class);

  @ClassRule
  public static final ResourceTestRule resources = ResourceTestRule.builder()
    .addResource(new AccountsResource(store))
    .build();

  private final Account firstAccount = new Account(1L, new BigDecimal(500));
  private final Account secondAccount = new Account(2L, new BigDecimal(500));

  @Before
  public void setup() {
    when(store.findById(eq(1L))).thenReturn(firstAccount);
    when(store.findById(eq(2L))).thenReturn(secondAccount);
  }

  @After
  public void tearDown() {
    reset(store);
  }

  @Test
  public void testGetAccount() {
    assertThat(resources.target("/accounts/1").request().get(Account.class))
      .isEqualTo(firstAccount);
    verify(store).findById(1L);
  }
  @Test
  public void testTransfer() {
    final MoneyTransfer transfer = new MoneyTransfer(2, new BigDecimal(100));
    final Response response = resources.target("/accounts/1/transfer").request(MediaType.APPLICATION_JSON_TYPE).post(Entity.entity(transfer, MediaType.APPLICATION_JSON_TYPE));

    //assertThat(response.getEntity()).isEqualTo(true);
    assertThat(store.findById(1L).getBalance()).isEqualTo("600");
    assertThat(store.findById(2L).getBalance()).isEqualTo("400");
  }
}
