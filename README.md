# Accounts Service

This is sample service demonstration money transfer between accounts build with Dropwizard.
Contains simple tests for account retrieval and money transfer. Includes limited validation for brewity.

## Run
```
java -jar accounts-service-1.0-SNAPSHOT.jar server accounts-service.yaml
```

## Data Model
Data model is extremely simple and consists of only one Entity:
### Account
* id - long number used as identification of Account
* balance - BigDecimal showing current available money

By default datastore is in-memory (presented by HSQLDB) and empty. You can configure it in _accounts-service.yaml_ 

## Available resources

### POST /accounts
```javascript
{
    "id":<accountId>,
    "balance":<accountBalance>
}
```
Creates account with given balance, returns id.

---

### GET /accounts/{id}

Returns account with given id.

---
### POST /accounts/{id}/transfer
```javascript
{
"sourceAccountId":<idOfTheAccountToTakeMoneyFrom>,
"amount":<amountToTransfer>
}
```

Transfers _amount_ of money from _sourceAccountId_ to _{id}_.
If sourceAccount does not have enough money returns _false_.